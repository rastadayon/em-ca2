clear
clc

Nx = 100; 
Ny = 100; 
Nz = 100; 
tol = 1d-6; 
err = 1;    

u = rand(Nx, Ny, Nz) *10;
for i = 1: Nx
   for j = 1:Ny
       u(i,j,Nz) = 3;
       u(i,j,1) = 0;
   end
end
for i = 1: Ny
   for j = 1:Nz
       u(1,i,j) = 0;
       u(Nx,i,j) = 0;
   end
end
for i = 1: Nx
   for j = 1:Nz
       u(i,1,j) = 0;
       u(i,Ny,j) = 0;
   end
end

ukp1 = u;

while(err > tol)
    
    for k = 2 : Nz - 1
       for j = 2 : Ny - 1
          for i = 2: Nx - 1
             ukp1(i,j,k) = 1/6 *(u(i+1,j,k)+ u(i-1,j,k) + u(i,j+1,k) + u(i,j-1,k) + u(i,j,k+1) + u(i,j,k-1)); 
          end
       end
    end
    err = sqrt(sum(sum((ukp1 - u).^2)));
    u = ukp1;
end



figure
contour(1:1:Nx, 1:1:Ny, u(:,:,50));
title('z equal to 50 (4 in real measurements)');
xlabel('x');
ylabel('y');

new_u = zeros(Nx,Nz);
for i = 1:Nx
   for k = 1: Nz
      new_u(i,k) = u(i,50,k); 
   end
end
figure
contour(1:1:Nx, 1:1:Nz, new_u);
title('y equal to 50 (4 in real measurements)');
xlabel('x');
ylabel('z');


new_u = zeros(Nx,Nz);
for j = 1:Ny
   for k = 1: Nz
      new_u(j,k) = u(50,j,k); 
   end
end
figure
contour(1:1:Ny, 1:1:Nz, new_u);
title('x equal to 50 (4 in real measurements)');
xlabel('y');
ylabel('z');

