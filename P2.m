clear;
clc;
x = linspace(0,1000,100);
t = linspace(0,1000,100);
m = 0;
sol = pdepe(m, @pdefun, @icfun, @bcfun, x, t);
u = sol(:,:,1);

figure
surf(x,t,u) 
title('Numerical solution.')
xlabel('Distance x')
ylabel('Time t')


x = linspace(0,1000,100);
t = linspace(0,1000,100);
m = 0;
sol2 = pdepe(m, @pdefunhomo, @icfun, @bcfun, x, t);
u = sol2(:,:,1);

figure
surf(x,t,u) 
title('Numerical solution for homogenous')
xlabel('Distance x')
ylabel('Time t')

function u = icfun(x)
u = x*(1-x);
end

function [c, f, s] = pdefun(x, t, u, DuDx)
c = 0.2;
f = DuDx;
s = exp(-t);
end

function [pl, ql, pr, qr] = bcfun(xl, ul, xr, ur, t)
pl = ul;
ql =  0;
pr = ur;  
qr = 0;
end

function [c, f, s] = pdefunhomo(x, t, u, DuDx)
c = 0.2;
f = DuDx;
s = 0;
end