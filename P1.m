h = 3;
c = 4;


model = createpde(1);
geometryFromEdges(model,@squareg);
pdegplot(model,'EdgeLabels','on'); 
ylim([-1.1 1.1]);
xlim([-1.1 1.1]);
title 'Geometry With Edge Labels Displayed';
xlabel x
ylabel y

% The Klein-Gordon PDE is like this:
%                       ?2u/?t2 = c^2 * ?2u/?x2 - hu
% The createpde makes an equation with this format:
%                       m?2u?t2 + d * ?u/?t - ??(c?u)+au = f
% therefore the m, a, c, f, d should be specified as follows: 

m = 1;
d = 0;
c = c*c;
a = h;
f = 0;

specifyCoefficients(model,'m',m,'d',d,'c',c,'a',a,'f',f);

applyBoundaryCondition(model,'dirichlet','Edge',[2,4],'u',0);
applyBoundaryCondition(model,'neumann','Edge',([1 3]),'g',0);

u0 = @(location) exp(location.x);
ut0 = @(location) 0;
setInitialConditions(model,u0,ut0);

generateMesh(model);
figure
pdemesh(model);
ylim([-1.1 1.1]);
axis equal
xlabel x
ylabel y


n = 31;
tlist = linspace(0,5,n);

model.SolverOptions.ReportStatistics ='on';
result = solvepde(model,tlist);


u = result.NodalSolution;
figure
umax = max(max(u));
umin = min(min(u));
for i = 1:31
    pdeplot(model,'XYData',u(:,i),'ZData',u(:,i),'ZStyle','continuous',...
                  'Mesh','off','XYGrid','on','ColorBar','off');
    axis([-1 1 -1 1 umin umax]);
    caxis([umin umax]);
    grid on
    xlabel x
    ylabel t
    zlabel u
    M(i) = getframe;
end





%------------------------ part 2--------------------

model2 = createpde(1);
geometryFromEdges(model2,@squareg);

% The Klein-Gordon PDE is like this:
%                       ?2u/?t2 = c^2 * ?2u/?x2 - hu
% The createpde makes an equation with this format:
%                       m?2u?t2 + d * ?u/?t - ??(c?u)+au = f
% therefore the m, a, c, f, d should be specified as follows: 

m = 1;
d = 0;
c = c*c;
a = 0;
f = 0;

specifyCoefficients(model2,'m',m,'d',d,'c',c,'a',a,'f',f);

applyBoundaryCondition(model2,'dirichlet','Edge',[2,4],'u',0);
applyBoundaryCondition(model2,'neumann','Edge',([1 3]),'g',0);

u0 = @(location) exp(location.x);
ut0 = @(location) 0;
setInitialConditions(model2,u0,ut0);

generateMesh(model2);
figure
pdemesh(model2);
ylim([-1.1 1.1]);
axis equal
xlabel x
ylabel y


n = 31;
tlist = linspace(0,5,n);

model2.SolverOptions.ReportStatistics ='on';
result = solvepde(model2,tlist);


u = result.NodalSolution;
figure
umax = max(max(u));
umin = min(min(u));
for i = 1:31
    pdeplot(model,'XYData',u(:,i),'ZData',u(:,i),'ZStyle','continuous',...
                  'Mesh','off','XYGrid','on','ColorBar','off');
    axis([-1 1 -1 1 umin umax]);
    caxis([umin umax]);
    grid on
    xlabel x
    ylabel t
    zlabel u
    M(i) = getframe;
end
